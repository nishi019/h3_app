import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSelection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text('GIRAFFE IS THE CUTEST!!',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                  ),
                ),
              ),
              Text('If you like Giraffes, tap the Favorite button below!! '
                'You can save this if you tap the flag -->',
              softWrap: true,
              style: TextStyle(
                color: Colors.grey[500]),
              ),
            ],
          ),
          ),
          FlagWidget(),
        ],
      ),
    );
    Widget buttonSelection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FavoriteWidget(),
        ],
      ),
    );
    return MaterialApp(
      title: 'Homework3 ',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Homework3'),
          ),
          body: ListView(
              children: [
                titleSelection,
                Image.asset(
                  'image/giraffe.jpeg',
                  width: 600,
                  height: 240,
                  fit: BoxFit.cover,
                ),
                buttonSelection,
              ]
          ),
          ),
      );
  }
}
class FlagWidget extends StatefulWidget {
  @override
  _FlagWidgetState createState() => _FlagWidgetState();
}
class _FlagWidgetState extends State<FlagWidget> {
  bool _isFlag = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFlag ? Icon(Icons.flag) : Icon(Icons.outlined_flag)),
            color: Colors.blueAccent,
            iconSize: 40,
            onPressed: _toggleFlag,
          ),
        ),
      ],
    );
  }
  void _toggleFlag() {
    setState(() {
      if(_isFlag) {
        _isFlag = false;
      } else {
        _isFlag= true;
      }
    });
  }
}
class FavoriteWidget extends StatefulWidget {
   @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}
class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = false;
  int _favoriteCount = 1;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            iconSize: 70,
            color: Colors.redAccent,
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 8,
          child: Container(
            child: Text('$_favoriteCount', style: TextStyle(fontSize: 70),),
          ),
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}

